'''
Created on Oct 24, 2020

@author: simonray
'''

#!/usr/bin/python

import numpy as np
import sys, getopt
import pandas as pd
import os
import csv

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqUtils import GC

def main(argv):
    global windowSize
    global shRNAlen
    global shRNAs 
    global outputFile
    
    shRNAs = {}
    genomeFile = ''
    windowSize = 0
    shRNAlen = 21
    outputFile = ""
    try:
        opts, args = getopt.getopt(argv, "hm:r:o:", ["mirfile=","rnafile=","outputfile="])
    except getopt.GetoptError:
        print ('HSVshRNA.py -g <genomefile> -r <windowsize> -l shrnalen -o <outputfile>')
        sys.exit()
    for opt, arg in opts:
        if opt == '-h':
            print ('HSVshRNA.py -g <genomefile> -r <windowsize> -l shrnalen -o <outputfile>')
            sys.exit()
        elif opt in ("-g", "--genomefile"):
            genomeFile = arg
        elif opt in ("-r", "-windowsize"):
            windowSize = arg
        elif opt in ("-l", "-shrnalen"):
            shRNAlen = arg
        elif opt in ("-o", "-outputfile"):
            outputFile = arg
            
    print("genomefile is <" + genomeFile + ">")
    print("windowsize is <" + windowSize + ">")
    print("outfile is <" + outputFile + ">")
    
    procGenome(genomeFile)
    
    
def procGenome(genFile):    
    """
    load the genome sequence in FASTA format. If more than one record is found,
    process each record one at a time.
    """
    for record in SeqIO.parse(genFile, "fasta"):
        genomeSeq = print(record.id)
        scanSeq(genomeSeq)
        
    
    
    
def scanSeq(genSeq):
    """
    scan the genome for a set of shRNAs by breaking it down into a set of non-overlapping
    tiles and finding the best shRNA within each tile.
    """
    tileNumber = 0
    bestLDScores = {}
    genLen = len(genSeq)
    numberOfTiles = int(genLen/windowSize) + 1
    startPos = 0
    while tileNumber<numberOfTiles:
        tileLDScores = {}
        while startPos<(windowSize-shRNAlen):
            startPos = tileNumber*windowSize
            shFrag = genSeq.seq[startPos:startPos+shRNAlen]
            fragScore = getLDScore(shFrag)
            tileLDScores[startPos]=fragScore
            
            startPos=startPos + 1
        
        bestStartPos = getBestShRNA(tileLDScores, tileNumber)
        shRNA[bestStartPos] = {getLDScore(genSeq.seq[bestStartPos:bestStartPos+shRNAlen])}
        tileNumber=tileNumber+1
    
    
    
def getBestShRNA(ldScores, tileNumber):
    """
    find the shRNA with the best LDscore within this list.
    if we get more than one shRNA with the same score, we
    take the one closest to the middle of the list (because
    we want these shRNAs to be most evenly spaced across the
    genome.
    
    We may also need to check the same (or very similar) sequence
    doesn't already exist in the list of accepted shRNAs
    """
    bestHits = {}
    bestScore = 0
    bestStart = 0
    for startPos, score in ldScores.items():
        if  score > bestScore:
            bestScore = score
            bestStart = bestStart
            bestHits = {}
        elif score == bestScore:
            bestHits[startPos] = score
    
    # if we have more than one best hit, we need to filter further
    # Here, we find the shRNA closest to the middle of the window
    # Note, we could still get a situation where we have matches
    # on either side of the midpoint. In this case, we take the first
    # one that was found.               
    if(bestHits.__sizeof__()>1):
        minDist = windowSize/2
        tileNumber
        for startPos, score in bestHits.items():
            if(abs(startPos - tileNumber*windowSize) < minDist):
                minDist = abs(startPos - tileNumber*windowSize)
                bestStart = startPos

    return bestStart



    
def getLDScore(seqString):    
    """Generates a score for the seqString that gives an indication of its suitability
       to function as a short hairpin RNA (shRNA)
       This score is based on information provided by the 
       `Broad Institute <https://portals.broadinstitute.org/gpp/public/resources/rules>`
       and from the following papers.
    
    """   
    seqFrag = Seq(seqString)
    gcCount = GC(seqFrag)
    return gcCount
    

def writeBestShRNAs():
    with open(outputFile, 'w') as f:  # Just use 'w' mode in 3.x
        w = csv.DictWriter(f, shRNAs.keys())
        w.writeheader()
        w.writerow(shRNAs)    